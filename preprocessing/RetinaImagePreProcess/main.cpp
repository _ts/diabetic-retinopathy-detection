#include <iostream>
#include <algorithm>
#include <filesystem>
#include <list>
#include <mutex>
#include <stack>
#include <future>
#include <csignal>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;
namespace fs = std::filesystem;

/**
 * Inspired by https://www.kaggle.com/tanlikesmath/diabetic-retinopathy-resnet50-binary-cropped.
 */

//constexpr auto IMG_SIZE = 512;
//constexpr auto ENV_SRC_DIR = "DIABRETI_SRC_DIR";
//constexpr auto ENV_DST_DIR = "DIABRETI_DST_DIR";

/**
 * Safe parallel stack, where a pair of src and dst file paths reflect a task.
 */
class Files {
private:
	stack<fs::path, vector<fs::path>> src;
	stack<fs::path, vector<fs::path>> dst;
	mutex                             fileMutex;

public:
	Files(vector<fs::path> srcFiles, vector<fs::path> dstFiles) : src(srcFiles), dst(dstFiles) {
		if (srcFiles.size() != dstFiles.size()) {
			throw length_error("src files != dst files");
		}
	}

	/**
	 * @return an optional containing a pair of source and destination file. If std::nullopt is returned, no files remain.
	 */
	optional<pair<fs::path, fs::path>> get(int &remaining) {
		optional<pair<fs::path, fs::path>> r;

		fileMutex.lock();
		{
			if (src.empty()) {
				r         = nullopt;
				remaining = 0;
			} else {
				r         = optional{pair(src.top(), dst.top())};
				remaining = src.size();

				src.pop();
				dst.pop();
			}
		}
		fileMutex.unlock();

		return r;
	}
};

Mat circle_mask;

bool APPLY_CLAHE;
int  IMG_SIZE;

void modify(const fs::path &path_in, const fs::path &path_out) {
	auto img = cv::imread(path_in.string()); // read image in BGR colorspace

	if (img.rows < IMG_SIZE || img.cols < IMG_SIZE) {
		// don't scale up
		return;
	}

	Mat lab;
	cvtColor(img, lab, COLOR_BGR2Lab);

	// extract L channel
	Mat l;
	extractChannel(lab, l, 0);

	Mat mask;
	// apply threshold on lightness channel
	threshold(l, mask, 10, 255, THRESH_BINARY);

	// find contours
	vector<vector<Point>> contours;
	findContours(mask, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

	if (contours.size() == 0) {
		cout << "File: " + path_in.string() + ": No contours found!" << endl;
		return;
	}

	// select largest contour by area
	auto contour_it = max_element(contours.begin(), contours.end(),
								  [](const vector<Point> &v1, const vector<Point> &v2) {
									  return contourArea(v1) < contourArea(v2);
								  });

	// determine a clean circle around contour
	auto r      = boundingRect(*contour_it);
	auto center = Point2f(r.x + r.width / 2, r.y + r.height / 2);
	auto radius = max(r.height, r.width) / 2;

	if (radius < (lab.rows / 3)) {
		cout << "File: " + path_in.string() + ": Retina too small or not found" << endl;
		return;
	}

	auto retina_rect  = Rect2i(Point(center.x - radius, center.y - radius),
							   Point(center.x + radius, center.y + radius));
	auto content_rect = retina_rect & Rect2i(Point(0, 0), lab.size()); // intersect

	// clip image to relevant content
	lab = lab(content_rect);

	// some images contain a clipped retina -> expand the image to fit a square
	if (content_rect.height < retina_rect.height) {
		int top    = -(retina_rect.y / 2);
		int bottom = -retina_rect.y - top;

		copyMakeBorder(lab, lab, top, bottom, 0, 0, BORDER_CONSTANT, Scalar(0, 128, 128));
		// (0, 128, 128) won't equal black, duo lossy conversion
	}

	resize(lab, lab, Size(IMG_SIZE, IMG_SIZE), 0, 0, INTER_AREA);

	// apply clahe on lightness channel
	if (APPLY_CLAHE) {
		// extract L channel again
		extractChannel(lab, l, 0);

		auto clahe = createCLAHE(4.0, Size(12, 12));
		clahe->apply(l, l);

		// overwrite L channel
		insertChannel(l, lab, 0);
	}

	// convert LAB to BGR
	cvtColor(lab, img, COLOR_Lab2BGR);

	// remove noise surrounding the retina
	multiply(img, circle_mask, img);

	imwrite(path_out.string(), img);
}

volatile atomic_bool running = true;

void work(Files &files) {
	while (running == true) {
		int  remaining = 0;
		auto f         = files.get(remaining);

		cout << "remaining: " << remaining << endl;

		if (f.has_value()) {
			modify(f.value().first, f.value().second);
		} else {
			return;
		}
	}
}

void setup() {
	// create a circular mask
	circle_mask = Mat::zeros(Size(IMG_SIZE, IMG_SIZE), CV_8UC3);
	circle(circle_mask, Point(circle_mask.size() / 2), IMG_SIZE / 2, Scalar(1, 1, 1), FILLED);
}

void interrupt_handler(int signal) {
	std::signal(signal, &interrupt_handler);

	cout << "received signal" << endl;
	running = false;
}

int main(int argc, char **argv) {
	// just register everything relevant
	std::signal(SIGINT, &interrupt_handler);
	std::signal(SIGTERM, &interrupt_handler);
	std::signal(SIGQUIT, &interrupt_handler);
	std::signal(SIGKILL, &interrupt_handler);
	std::signal(SIGHUP, &interrupt_handler);

	const string keys =
						 "{help |      | print this message                     }"
						 "{dbg  |      | debug                                  }"
						 "{src  |<none>| source path                            }"
						 "{dst  |<none>| destination path                       }"
						 "{exc  |      | excludes already existing files in dst }"
						 "{clahe|True  | apply clahe on final image             }"
						 "{size |512   | final image size                       }";

	CommandLineParser parser(argc, argv, keys);

	parser.about("RetinaImagePreProcess");
	if (parser.has("help")) {
		parser.printMessage();
		return 0;
	}
	auto env_src = parser.get<string>("src");
	auto env_dst = parser.get<string>("dst");

	APPLY_CLAHE = parser.get<bool>("clahe");
	IMG_SIZE    = parser.get<int>("size");

	if (parser.has("dbg")) {
		setup();
		modify(fs::path(env_src), fs::path(env_dst));

		return 0;
	}

	if (!parser.check()) {
		parser.printErrors();
		return -1;
	}

//	auto env_src = getenv(ENV_SRC_DIR);
//	if (env_src == nullptr) {
//		throw domain_error("environment variable " + string(ENV_SRC_DIR) + " not set");
//	}
//	auto env_dst = getenv(ENV_DST_DIR);
//	if (env_dst == nullptr) {
//		throw domain_error("environment variable " + string(ENV_DST_DIR) + " not set");
//	}

	auto psrc = fs::path(env_src);
	auto pdst = fs::path(env_dst);

	vector<fs::path> fsrc;
	vector<fs::path> fdst;

	if (parser.has("exc")) {
		vector<fs::path> exc_fdst;

		for (auto &entry : fs::directory_iterator(pdst)) {
			exc_fdst.push_back(entry.path().filename());
		}

		for (auto &entry : fs::directory_iterator(psrc)) {
			if (find(exc_fdst.begin(), exc_fdst.end(), entry.path().filename()) == exc_fdst.end()) {
				fsrc.push_back(entry.path());
				fdst.push_back(pdst / entry.path().filename());
			}
		}
	} else {
		for (auto &entry : fs::directory_iterator(psrc)) {
			fsrc.push_back(entry.path());
			fdst.push_back(pdst / entry.path().filename());
		}
	}

	// setup resources used across threads
	setup();

	// task stack
	auto files = Files(fsrc, fdst);

	vector<future<void>> futures;

	for (int i = 0; i < 32; i++) {
		// safe ref, since we don't leave the scope
		futures.push_back(async(launch::async, work, ref(files)));
	}

	// join
	for (auto &f : futures) {
		f.wait();
	}

	cout << "stopped" << endl;

	return 0;
}