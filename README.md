# Diabetic Retinopathy Detection

This project is part of a course work at the HTW Berlin and aims to explore several aspects of machine learning.
The objective is to classify retinal images from the [*Diabetic Retinopathy Detection*](https://www.kaggle.com/c/diabetic-retinopathy-detection/) competition, whether they show signs of diabetic retinopathy.

The model used to train and predict, is a more or less simplified [EfficientNet](https://arxiv.org/abs/1905.11946) implementation written in Python 3 using Tensorflow 2.1.

## Usage

All required files (images of retina, CSV files with labels) can be found under https://www.kaggle.com/c/diabetic-retinopathy-detection/
and https://www.kaggle.com/c/diabetic-retinopathy-detection/discussion/16149.

### Preprocessing

#### *Build*
```bash
$ cd ./preprocessing/RetinaImagePreProcess
$ mkdir build
$ cd build
$ cmake ..
$ make -j4
```

Download the files from [Diabetic Retinopathy Detection](https://www.kaggle.com/c/diabetic-retinopathy-detection/) and extract them to ```train_src``` and ```test_src```.

*Hint: ```cat files*.zip > file.zip``` joines the train/ test zip files back together*

#### *Run*
```bash
$ mkdir train_dst
$ mkdir test_dst
...
$ ./ripp --src="/path/to/train_src" --dst="/path/to/train_dst" --exc
$ ./ripp --src="/path/to/test_src" --dst="/path/to/test_dst" --exc
```

```--exc``` excludes files already in dst.

### Model (GPU)

Launch the notebook in e.g. Google Colab.
*Hint: The notebook should work out of the box in Google Colab using Google Drive to host the preprocessed images and save the checkpoints.
All paths start with ```/content/...``` adressing the home folder within Colab, and ```/content/drive/My Drive/retina/...``` adressing GDrive. Change these paths if you want to run the notebook locally.*

Unzip and place the files according to the directory structure given in the notebook.

### Model (TPU) **Work In Progress**

This requires some additional steps but is worth a try :) So far, the TPU experience in Colab is quite limited, so we use Google Cloud and host our own instance with a TPU:

Within gcloud console:
```bash
$ ctpu up \
    -disk-size-gb 20 \        # change as desired
    -tf-version 2.1 \         # TPU compatible with TF 2.1
    -preemptible \            # optional
    -preemptible-vm \         # optional
    -name MY_TPU \            # name of the VM and TPU; can't be changed afterwards!
    -gce-image https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-10-buster-v20191210 \ # since we require python >= 3.6
    -vm-only                  # idling a TPU is expensive, so for now, we only require an VM to work with
```

Login to your VM over ssh and setup a ```virtualenv``` and ```pip install tensorflow==2.1rc2```.

***Required if you want to work with Colab:***

Next install jupyter ```pip install jupyter jupyter_http_over_ws``` and run:
```bash
$ jupyter serverextension enable --py jupyter_http_over_ws
$ jupyter notebook \
    --NotebookApp.allow_origin='https://colab.research.google.com' \
    --port=8888 \
    --NotebookApp.port_retries=0
```

On your local machine (requires gcloud tools installed) forward traffic from 8888 to 8888 on the VM:
```bash
$ gcloud compute ssh --zone ZONE_OF_MY_TPU MY_TPU -- -L 8888:localhost:8888
```
Now go to https://colab.research.google.com/ and connect to the local runtime.

#### Creat tf records and a gcs bucket

Run the notebook for converting the jpegs into tf record files. The reason for this is, that accessing files from the TPU is (only) possible through Google Cloud Storage. Accessing a lot of small files on GCS is extremely slow, so a single tf record will pack hundreds of jpegs.

#### Run

Within gcloud console start the TPU:
```bash
$ ctpu up \
    -name MY_TPU \
    -tf-version 2.1 \
    -tpu-only # start the tpu only, vm already running
```

Finally launch the TPU notebook.

#### Cleanup

Run ```ctpu pause -name MY_TPU```.

# Acknowledgments
Thanks to [EyePACS](http://www.eyepacs.com/) for providing the retinal images for the [original competition](https://www.kaggle.com/c/diabetic-retinopathy-detection).

Cuadros, J., & Bresnick, G. (2009). EyePACS: an adaptable telemedicine system for diabetic retinopathy screening. _Journal of diabetes science and technology_, _3_(3), 509–516.

